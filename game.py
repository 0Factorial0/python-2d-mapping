def game():
    import sys,tty,termios
    from time import sleep
    from clear import clear

    #get user input
    class _Getch:
        def __call__(self):
            fd = sys.stdin.fileno()
            old_settings = termios.tcgetattr(fd)
            try:
                tty.setraw(sys.stdin.fileno())
                ch = sys.stdin.read(3)
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
            return ch

    #generate food
    def generate_food():
        import random
        #random tile
        random_number = random.randint(1,300)
        #if tile is empty than claim it
        if tile_map[random_number][0] == 0:
            tile_map[random_number][0] = 3
        else:
            #or generate again
            generate_food()

    #print win
    def print_win():
       #print menu
        print("-------------------------------------------------------------")
        print("Player Score: 100!")
        print("You Win!")
        print("-------------------------------------------------------------")


    #print the game
    def print_game(player_position, player_score):
        #clear game
        clear()

        if player_score == 100:
            print_win()
        else:
        #print menu
            print("-------------------------------------------------------------")
            print("Collect Foods To Gain Score (x)")
            print("Player Score: {0}!".format(player_score))
            print("-------------------------------------------------------------")

            #print canvas
            for i in range(0, 15):
                for j in range(1, 21):
                    index = (i*20)+j
                    if tile_map[index][0] == 0:
                        print("   ", end="")
                    elif tile_map[index][0] == 1:
                        print("[ ]", end="")
                    elif tile_map[index][0] == 2:
                        print(" o ", end="")
                    elif tile_map[index][0] == 3:
                        print(" x ", end="")
                print("")

            print("-------------------------------------------------------------")

            #get input
            inkey = _Getch()
            while(1):
                    k = inkey()
                    if k!='':break
            try:
                #if it's up arrow
                if k == '\x1b[A':
                    if tile_map[player_position-20][0] == 1:
                        pass
                    else:
                        if tile_map[player_position-20][0] == 3:
                            player_score += 1
                            generate_food()
                        tile_map[player_position] = [0]
                        player_position -= 20
                        tile_map[player_position] = [2]
                #if it's down arrow
                elif k == '\x1b[B':
                    if tile_map[player_position+20][0] == 1:
                        pass
                    else:
                        if tile_map[player_position+20][0] == 3:
                            player_score += 1
                            generate_food()
                        tile_map[player_position] = [0]
                        player_position += 20
                        tile_map[player_position] = [2]
                #if it's right arrow
                elif k == '\x1b[C':
                    if tile_map[player_position+1][0] == 1:
                        pass
                    else:
                        if tile_map[player_position+1][0] == 3:
                            player_score += 1
                            generate_food()
                        tile_map[player_position] = [0]
                        player_position += 1
                        tile_map[player_position] = [2]
                #if it's left arrow
                elif k == '\x1b[D':
                    if tile_map[player_position-1][0] == 1:
                        pass
                    else:
                        if tile_map[player_position-1][0] == 3:
                            player_score += 1
                            generate_food()
                        tile_map[player_position] = [0]
                        player_position -= 1
                        tile_map[player_position] = [2]
            #if you somehow get out of the canvas, return to the starting point
            except:
                player_position = 150

        #print again
        print_game(player_position, player_score)

    #tilemap
    tile_map = [0,  [1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],
                    [1],[0],[0],[0],[0],[0],[0],[0],[0],[0],[1],[0],[0],[0],[0],[0],[0],[0],[0],[1],
                    [1],[0],[0],[0],[0],[0],[0],[0],[0],[0],[1],[0],[0],[0],[0],[0],[0],[0],[0],[1],
                    [1],[0],[0],[0],[0],[0],[0],[0],[0],[0],[1],[0],[0],[0],[0],[0],[0],[0],[0],[1],
                    [1],[0],[0],[0],[0],[0],[0],[0],[0],[0],[1],[0],[0],[1],[1],[1],[1],[1],[1],[1],
                    [1],[0],[0],[0],[3],[0],[0],[0],[0],[0],[1],[0],[0],[0],[0],[1],[0],[0],[0],[1],
                    [1],[1],[1],[1],[1],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[1],[0],[0],[0],[1],
                    [1],[0],[0],[0],[0],[0],[0],[0],[0],[2],[0],[0],[0],[0],[0],[1],[0],[0],[0],[1],
                    [1],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[3],[0],[0],[0],[0],[0],[0],[1],
                    [1],[0],[0],[0],[0],[0],[0],[1],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[1],
                    [1],[0],[1],[1],[1],[0],[3],[1],[0],[0],[0],[0],[1],[0],[0],[0],[0],[0],[0],[1],
                    [1],[0],[0],[0],[1],[0],[0],[1],[0],[0],[0],[0],[1],[0],[0],[3],[0],[0],[0],[1],
                    [1],[0],[0],[0],[0],[0],[0],[1],[0],[0],[0],[0],[1],[0],[0],[0],[0],[0],[0],[1],
                    [1],[0],[0],[0],[0],[0],[0],[1],[0],[0],[0],[0],[1],[0],[0],[0],[0],[0],[0],[1],
                    [1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1],[1]]

    #define player
    player_position = 150
    player_score = 0

    #start the game
    print_game(player_position, player_score)

game()
